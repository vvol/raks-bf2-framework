# RaKS BF2 Framework - pUbLiC ReLeAsE v1.5

For BF2 v1.50 - Working on Battlelog.co Servers.

Credits: UC-FORUM

Compile with Visual C++ 2005 Express Edition & DX SDK 2010 !!
Punkbuster PBSS Proof - Returns Black SS

### ESP Features:
* Nametags
* Distancetags
* Healthbars
* Player Class
* Player Posture
* Snaplines
* Skeleton ESP
* Box ESP

### Team Features:
* Team Swap
* Force Commander
* Force Squad Leader
* Jump Squads

### Chams:
* USMC Soldiers
* CHINA Soldiers
* MEC Soldiers
* EURO Soldiers
* Vehicles
* Weapons
* Ground Weapons
* Explosives
* Hands

### Aimbot Features:
* No Spread
* No Shake
* AutoShoot
* Limit Aim Angle
* Draw Aim FOV
* Aim at Vehicles
* Auto Knife [Enabled by Default if Aimbot is Enabled] (Select Knife as Weapon, Hold Aimkey when Near Enemy) 


### Extra Features:
* Minimap
* Crosshair
* Speedhack
* Unlock Weapons
* Enemy Nearby
* Aim Warnings
* No Fog
* No Sky
* No TV Static
* Draw FPS

Known Bugs:
Vehicle Aimbot Still Not Working Correctly. [Jets & TV Missile Working Fine]
Aimbot Wont Aim Through See Through Fences.

---

```
Menu Key - Delete
AimKey = Middle Mouse

Speedhack - Forward = W
Speedhack - Backwards = S
Speedhack - Increase Speed = LEFT CTRL + ADD
Speedhack - Decrease Speed = LEFT CTRL + SUBTRACT

Team Swap Key - F5
Force Commander - F6
Force Squad Leader - F7
Cycle Through Squads (Jump Squad) - F8


Aimbot Key Codes:
Change via Settings File (EA Games\Battlefield 2\RAXBF2Framework.ini)

Example - Default Aimkey is Left Ctrl. [Aimkey=0xA2]

VK_LBUTTON   0x01
VK_RBUTTON   0x02
VK_CANCEL    0x03
VK_MBUTTON   0x04  
VK_BACK      0x08
VK_TAB       0x09
VK_ESCAPE    0x1B
VK_SPACE     0x20
VK_NEXT      0x22
VK_END       0x23
VK_HOME      0x24
VK_LEFT      0x25
VK_UP        0x26
VK_RIGHT     0x27
VK_DOWN      0x28
VK_PRINT     0x2A
VK_EXECUTE   0x2B
VK_INSERT    0x2D
VK_DELETE    0x2E
VK_NUMPAD0   0x60
VK_NUMPAD1   0x61
VK_NUMPAD2   0x62
VK_NUMPAD3   0x63
VK_NUMPAD4   0x64
VK_NUMPAD5   0x65
VK_NUMPAD6   0x66
VK_NUMPAD7   0x67
VK_NUMPAD8   0x68
VK_NUMPAD9   0x69
VK_MULTIPLY  0x6A
VK_ADD       0x6B
VK_SEPARATOR 0x6C
VK_SUBTRACT  0x6D
VK_DECIMAL   0x6E
VK_DIVIDE    0x6F
VK_F1        0x70
VK_F2        0x71
VK_F3        0x72
VK_F4        0x73
VK_F5        0x74
VK_F6        0x75
VK_F7        0x76
VK_F8        0x77
VK_F9        0x78
VK_F10       0x79
VK_F11       0x7A
VK_F12       0x7B
VK_NUMLOCK   0x90
VK_SCROLL    0x91
VK_LSHIFT    0xA0
VK_RSHIFT    0xA1
VK_LCONTROL  0xA2
VK_RCONTROL  0xA3
```
